# Constitution of Collaborative Coder Coalition [CCC]

## 1. Charta

1. Each member of the group represents an autonomous individual and is responsible for any of her/his actions alone.
2. Official declarations, notifications or any official messages made by CCC to CCC members, non CCC members or other groups, must be stated by an official representative of the group only (see 3.5. Representatives).
3. Aggressive acts by members of CCC against other players or groups may be considered as individual acts by that player, if not announced officially by the CCC *Community Manager* or CCC *Group Manager* (see 3.5. Representatives) prior to the act taking place.
4. Official announcements made by CCC must at least be (1) made made public to the CCC membership (e.g. using slack) and (2) send to affected non CCC players (e.g. via screeps ingame message) or groups (e.g. via slack or to a representative of that group via ingame message).


## 2. Member Admission

1. Admission of new members requires approval of all members of all sectors (block of rooms around SK center rooms, framed by controlerless "highway" rooms) that candidate has rooms of.
2. The CCC *Secretary* will inform all affected members of such a case. Those members may or may not react on that notification within 3 days (min. 72h). A missing statement means approval of that candidate.
3. As soon as all affected members approved a candidate, it may join the alliance.
4. If there is a rejection, that candidate may not join until the rejection has been retracted or the candidate relocates all rooms in all sectors which are in common with that rejecting member.
5. If the candidate claims any new rooms during the admission process, affecting new sectors, its inhabitants will get notified and asked for approval, as defined in §2.2 and a new approval time period starts.
6. Candidates must be GCL3 or above. If below this, applicants are welcome to hang out in #CCC, use our public code, contribute, ask questions until they reach GCL3.


## 3. Responsibilities

### 3.1. Internal affairs

1. Addmission of new members must be processed by a CCC *Representative* or its deputy.
2. Society members must not intentionally harm the assets of another society member.
3. Society members must act in good faith to preserve the society's diplomatic agreements. (see §4)

### 3.2. External affairs

1. Official invitations must be made by a CCC *Representative* or its deputy.
2. Official declarations or messages to other groups must be delivered by a CCC *Representative* or its deputy.
3. Official declarations of war must be delivered by a CCC *Representative* or its deputy.
4. Official messages, other than defined above, must be made by a representative of the group.

### 3.3. Representatives

* Group Managers [Forkmantis]
  * Administrative access to all repositories
  * Constitutional amendment
  * Deputy for all other roles

* Secretary [Xephael]
  * Identity management (who is who)
  * Manage come/leave/change processes
  * Take care of internal issues
  * Manage internal announcements

* Architects [Karlthepagan]
* Deputy Architects [Spedwards, Chobobobo]
  * code review
  * push access to the dev branch

* Community Managers [Xephael]
  * Monitoring of other alliances
  * Cross alliance communication
  * Manage invitations
  * Manage come/leave/change processes
  * Responsible for advertisement and group growth
  * Responsible for public perception

* Product Managers [vacant]
  * Management of issue list
  * Solution validation
  * Closing of issues

### 3.4. Termination

1. Members not adhering to their responsibilities will be subject to the majority judgement of a disciplinary council composed of the other representatives.
2. Disciplinary judgements may be vetoed by a group manager.

## 4. Diplomatic Status

We try to be as neutral as possible. Excemptions are listed below.

### 4.1 Non Aggression Pacts
none

### 4.2 War
* The Hive [Hive]
* Greater North-Eastern Co-Prosperity Sphere [CoPS]

*Chobobobo, Architect, 28.05.2017*